<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="./css/style.css" rel="stylesheet">
        <script src="./js/function.js"></script>
        <title>Chargement</title>
    </head>
    <body class="bg_animated main_theme">
        <main class="main_popup_container">
            <div class="loading_popup main_popup">
                <progress class="pure-material-progress-circular"></progress>
                <p class="loading_msg">Chargement...</p>
            </div>
        </main>
    </body>
</html>