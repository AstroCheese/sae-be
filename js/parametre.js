/**
 * Recovering raspberry pi settings.
 */
async function getParametre()
{
    displayLoading("Récupération des paramètres...");

    //Print the wifi name
    const accesPoint = await NODERED_get("/get_AP");
    let network = document.getElementById("network");
    network.value=accesPoint["name"];

    
    const settings = await PHP_get("/PHP_API/get_settings.php");
    if (settings != null){
        if (settings["autoRemove"]){
            document.getElementById("auto_suppr").checked=true;
        }else{
            document.getElementById("auto_suppr").checked=false;
        }     
        
        const timeSettings = getReadableTimeAndUnit(settings["removeInterval"]);
        let timeConservation = document.getElementById("conserv");
        timeConservation.setAttribute('value',timeSettings["value"]);
        let altitude = document.getElementById("altitude");
        altitude.setAttribute('value',settings["altitude"]);

        let timeConservationUnit = document.querySelector('#comboBoxTpsSuppr option[value="' + timeSettings["unit"] + '"]');
        if(timeConservationUnit){
            timeConservationUnit.setAttribute('selected','selected');
        }
    }

    hideLoading();
}


/**
 * Update raspberry pi settings.
 */
async function postParametre()
{
    displayLoading("Mise à jour des paramètres...");

    const enableAutoRemove = document.getElementById("auto_suppr");
    const timeConservation = document.getElementById("conserv");
    const timeConservationUnit = document.getElementById("comboBoxTpsSuppr");
    const network = document.getElementById("network");
    const altitude = document.getElementById("altitude");


    if (timeConservation.validity.badInput === true) {
        hideLoading();
        displayError("Impossible de sauvegarder les paramètres", "L'intervalle de relevé de suppression des campagnes n'a pas été renseigné ou son format est incorrecte. Veuillez renseigner un nombre entier positif puis réessayez.");
        return;
    } 

    
    let data1 = await PHP_post("/PHP_API/set_settings.php", {
        "timeConservation": timeConservation.value,
        "timeConservationUnit": timeConservationUnit.value,
        "enableAutoRemove": enableAutoRemove.checked,
        "altitude":altitude.value
    });

    const raspberryNetwork = await NODERED_get("/get_AP");

    if(network.value!=null && network.value!=raspberryNetwork){
        if(network.value.lenght<=32 && network.value.lenght>0){
            if(network.value.match(/^[a-zA-Z0-9\s-_]+$/)){
                const data2 = await NODERED_post("/set_AP", {
                    "network": network.value,
                });
                if (await displayConfirm("Changement du nom du WIFI", "Vous avez changer le nom du WIFI de la cellule cependant pour que ce changement soit visible il faut redémarrer l'appareil. Cela entraînera l'arrêt de campagne en cours. Voulez-vous mettre à jour la date et l'heure de la cellule ?", 'Redémarrer la cellule', false) == true) {
                    //restart
                    const data3 = await NODERED_get("/restart");
                }   
            }else{
                displayError("Impossible de sauvegarder les paramètres", "Des caractères spéciaux et interdits sont utilisés pour le nouveau nom du réseau. Veuillez renseigner un nom de réseau sans caractère spéciaux puis réessayez.");
            }    
        }else{
            displayError("Impossible de sauvegarder les paramètres", "Le nouveau nom du réseau dépasse 32 caractères ou ne contient aucun caractère. Veuillez renseigner un nom de réseau entre 1 et 32 caractères.");
        }      
    }

    if(data1 != null){
        displaySuccess("Paramètres mis à jour !", "Les paramètres ont été mis à jour avec succès.");
    } 

    hideLoading();
}

/**
 * Delete all data of the Raspberry pi
 */
async function postDeleteAll()
{
    if (await displayConfirm('Voulez-vous vraiment supprimer toutes les données de cet appareil ?', 'Toutes les campagnes, mesures et paramètres seront supprimées définitivement. Cette action est irréversible.', 'Effacer', true) == true) {
        displayLoading("Suppression des données...");

        const securityKey="I_do_believe_I_am_on_fire"
        
        const data = await PHP_post("/PHP_API/reset.php", {
            "key": securityKey
        });

        if(data != null){
            displaySuccess("Données supprimées !", "Toutes les campagnes, mesures, logs et paramètres ont été supprimées avec succès.");
        }


        hideLoading();
        // redirect
        window.location = "/setup_time.php"
    }
}

document.addEventListener("DOMContentLoaded", () => {
    getParametre();
});